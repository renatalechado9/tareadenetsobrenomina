﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using Nomina.Views.Extensiones;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmEmpleado : Form
    {
        public DataSet DSNomina { get; set; }
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private DataRow drEmpleado;
        public int index;
        public int indexactual;
        public FrmEmpleado()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            InitializeComponent();
        }
        //MEtodo para permitir que los botones sean funcionables
        public void SetButtonEnable(bool flag1,bool flag2)
        {
            button1.Enabled = flag1;
            button2.Enabled = flag2;
        }
        //Metodo para guardar de un solo los empleados
        public void SetEmpleado(Empleado e,int index)
        {
            txtcedula.Text = e.Cedula;
            txtnombres.Text = e.Nombres;
            txtapellidos.Text = e.Apellidos;
            txtdireccion.Text = e.Direccion;
            txttelefono.Text = e.Telefono;
            txtsalario.Text = e.Salario.ToString();
            DatTimPickerContratacion.Value = e.FechaContratacion;
            this.index = index;
            this.indexactual = e.Id;
        }


        //Guardar datos
        private void button2_Click(object sender, EventArgs e)
        {

            drEmpleado = DSNomina.Tables["Empleado"].NewRow();
            //Creo el nuevo objeto empleado
            if (txtnombres.Text == "" || txtapellidos.Text == "" || txtcedula.Text == "" || txttelefono.Text == "" || txtdireccion.Text == "" || txtsalario.Text=="")
            {
                MessageBox.Show("Error, Debe de llenar todos los datos", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Empleado empleado = new Empleado()
            {
                Id = daoEmpleadoImplements.Guardarid() + 1,
                Nombres = txtnombres.Text,
                Apellidos = txtapellidos.Text,
                Cedula = txtcedula.Text,
                Telefono = txttelefono.Text,
                Direccion = txtdireccion.Text,
                Salario = Convert.ToDecimal(txtsalario.Text),
                FechaContratacion = DatTimPickerContratacion.Value
            };
            drEmpleado["Id"] = empleado.Id;
            drEmpleado["Cedula"] = empleado.Cedula;
            drEmpleado["Nombres"] = empleado.Nombres;
            drEmpleado["Apellidos"] = empleado.Apellidos;
            drEmpleado["Direccion"] = empleado.Direccion;
            drEmpleado["Telefono"] = empleado.Telefono;
            drEmpleado["FechaContratacion"] = empleado.FechaContratacion;
            drEmpleado["Salario"] = empleado.Salario;
            //PAra ver si hay un doble registro de Celula
           
            if (daoEmpleadoImplements.findByCedula(empleado.Cedula) != null)
            {
                MessageBox.Show("Error, Numero de cedula duplicada", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;    
            }// esto ya es para anadirlo a la tablita

            daoEmpleadoImplements.Create(empleado);
            DSNomina.Tables["Empleado"].Rows.Add(drEmpleado);
            Dispose();
        }
        //Editar datos
        private void button1_Click(object sender, EventArgs e)
        {
            drEmpleado = DSNomina.Tables["Empleado"].NewRow();
            //Creo el nuevo objeto empleado
            if (txtnombres.Text == "" || txtapellidos.Text == "" || txtcedula.Text == "" || txttelefono.Text == "" || txtdireccion.Text == "" || txtsalario.Text == "")
            {
                MessageBox.Show("Error, Debe de llenar todos los datos", "Mensaje de Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }
            Empleado empleado = new Empleado()
            {
                Id = indexactual,
                Nombres = txtnombres.Text,
                Apellidos = txtapellidos.Text,
                Cedula = txtcedula.Text,
                Telefono = txttelefono.Text,
                Direccion = txtdireccion.Text,
                Salario = Convert.ToDecimal(txtsalario.Text),
                FechaContratacion = DatTimPickerContratacion.Value
            };
            drEmpleado["Id"] = indexactual;
            drEmpleado["Cedula"] = empleado.Cedula;
            drEmpleado["Nombres"] = empleado.Nombres;
            drEmpleado["Apellidos"] = empleado.Apellidos;
            drEmpleado["Direccion"] = empleado.Direccion;
            drEmpleado["Telefono"] = empleado.Telefono;
            drEmpleado["FechaContratacion"] = empleado.FechaContratacion;
            drEmpleado["Salario"] = empleado.Salario;
            //Remueve el que ya existe
            daoEmpleadoImplements.Update(empleado);
            //Inserto en la misma posicion
            MessageBox.Show("Se hizo el update correctamente", "Warning", MessageBoxButtons.OK, MessageBoxIcon.Information);
            DSNomina.Tables["Empleado"].Rows.RemoveAt(index);
            DSNomina.Tables["Empleado"].Rows.InsertAt(drEmpleado, index);
            Dispose();
    }

        private void solodecimales(object sender, KeyPressEventArgs e)
        {
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.'))
            {
                e.Handled = true;
            }
            // solo 1 punto decimal
            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }
        }

        private void solotexto1(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }

        private void solotexto2(object sender, KeyPressEventArgs e)
        {
            if (Char.IsLetter(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsControl(e.KeyChar))
            {
                e.Handled = false;
            }
            else if (Char.IsSeparator(e.KeyChar))
            {
                e.Handled = false;
            }
            else
            {
                e.Handled = true;
            }
        }
    }
}
