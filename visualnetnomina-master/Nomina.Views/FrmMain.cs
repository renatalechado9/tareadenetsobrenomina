﻿using Nomina.Business.Entities;
using Nomina.Business.Implements;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Nomina.Views
{
    public partial class FrmMain : Form
    {
        private DaoEmpleadoImplements daoEmpleadoImplements;
        private List<Empleado> empleados;
        private BindingSource bsEmpleados;
        public FrmMain()
        {
            daoEmpleadoImplements = new DaoEmpleadoImplements();
            bsEmpleados = new BindingSource();
            InitializeComponent();
        }

        private void FrmMain_Load(object sender, EventArgs e)
        {
            empleados = daoEmpleadoImplements.All();

            empleados.ForEach(ep => {
                dsNomina.Tables["Empleado"].Rows.Add(ep.EmpleadoAsArray());
            });

            bsEmpleados.DataSource = dsNomina;
            bsEmpleados.DataMember = dsNomina.Tables["Empleado"].TableName;

            dgvEmpleados.DataSource = bsEmpleados;
            dgvEmpleados.AutoGenerateColumns = true;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            FrmEmpleado frmEmpleado = new FrmEmpleado();
            frmEmpleado.DSNomina = dsNomina;
            //Esto es para solo permitir que guarde
            frmEmpleado.SetButtonEnable(false, true);
            frmEmpleado.ShowDialog(this);
        }

        private void txtbusqueda_TextChanged(object sender, EventArgs e)
        {
            //La Dataview sirve para buscar, filtrar y todo ese tipo de cosas
            DataView data = dsNomina.Tables[0].DefaultView;
            data.RowFilter = "Nombres LIKE '%" + txtbusqueda.Text.Trim() + "%'" + "OR Apellidos LIKE '%" + txtbusqueda.Text.Trim()+"%'" + "OR Cedula LIKE '%" + txtbusqueda.Text.Trim() + "%'";
            dgvEmpleados.DataSource = data;
            //dgv empleados es la dataGridView
            //Y la datasource hace un get o un set con respecto a la datagridview
        }
        //Editar 
        private void button2_Click(object sender, EventArgs e)
        {
            //Esto es para permitir que haga una seleccion de fila para que si le da el boton edit habra esa informacion. Y si no esta
            //seleccionado pues mandar un mensaje de error 
            if(dgvEmpleados.SelectedRows.Count > 0)
            {
                //Son los seleccionados
                int index = dgvEmpleados.SelectedRows[0].Index;
                int id = Convert.ToInt32(dgvEmpleados.Rows[index].Cells["Id"].Value.ToString());
                string cedula = dgvEmpleados.Rows[index].Cells["Cedula"].Value.ToString();
                string nombres = dgvEmpleados.Rows[index].Cells["Nombres"].Value.ToString();
                string apellidos = dgvEmpleados.Rows[index].Cells["Apellidos"].Value.ToString();
                string direccion = dgvEmpleados.Rows[index].Cells["Direccion"].Value.ToString();
                string telefono = dgvEmpleados.Rows[index].Cells["Telefono"].Value.ToString();
                DateTime fechadecontratacion = Convert.ToDateTime(dgvEmpleados.Rows[index].Cells["FechaContratacion"].Value.ToString());
                decimal salario = Convert.ToDecimal(dgvEmpleados.Rows[index].Cells["Salario"].Value.ToString());
                //Ahora creo el empleado
                Empleado empleado = new Empleado()
                {
                    Id = id,
                    Nombres = nombres,
                    Apellidos = apellidos,
                    Cedula = cedula,
                    Telefono = telefono,
                    Direccion = direccion,
                    Salario = salario,
                    FechaContratacion = fechadecontratacion
                };
                //LO envio para que aparezca de un solo en la pantalla de frmempleado
                FrmEmpleado frm = new FrmEmpleado();
                frm.SetEmpleado(empleado, index);
                frm.DSNomina = dsNomina;
                frm.SetButtonEnable(true, false);
                frm.ShowDialog();
            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila para editar", "Error",MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        //eliminar datos
        private void button3_Click(object sender, EventArgs e)
        {
            //Ocupe lo mismo del boton editar
            if (dgvEmpleados.SelectedRows.Count > 0)
            {
                //Son los seleccionados
                int index = dgvEmpleados.SelectedRows[0].Index;
                int id = Convert.ToInt32(dgvEmpleados.Rows[index].Cells["Id"].Value.ToString());
                string cedula = dgvEmpleados.Rows[index].Cells["Cedula"].Value.ToString();
                string nombres = dgvEmpleados.Rows[index].Cells["Nombres"].Value.ToString();
                string apellidos = dgvEmpleados.Rows[index].Cells["Apellidos"].Value.ToString();
                string direccion = dgvEmpleados.Rows[index].Cells["Direccion"].Value.ToString();
                string telefono = dgvEmpleados.Rows[index].Cells["Telefono"].Value.ToString();
                DateTime fechadecontratacion = Convert.ToDateTime(dgvEmpleados.Rows[index].Cells["FechaContratacion"].Value.ToString());
                decimal salario = Convert.ToDecimal(dgvEmpleados.Rows[index].Cells["Salario"].Value.ToString());
                //Ahora creo el empleado
                Empleado empleado = new Empleado()
                {
                    Id = id,
                    Nombres = nombres,
                    Apellidos = apellidos,
                    Cedula = cedula,
                    Telefono = telefono,
                    Direccion = direccion,
                    Salario = salario,
                    FechaContratacion = fechadecontratacion
                };
                //Hacer lo de la verificacion atraves de una ventana
                if (MessageBox.Show("¿Desea eliminar el registro de este empleado?", "WARNING", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
                {
                    //Esto es para eliminar al empleado
                    dgvEmpleados.Rows.RemoveAt(index);
                    daoEmpleadoImplements.Delete(empleado);
                }

            }
            else
            {
                MessageBox.Show("Debe seleccionar una fila para eliminar", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
